# Open Data

This repository contains code to produce curated open data assets for the Cook County Open Data Portal. The primary purpose of this repository is to provide tracability from open data assets, back to internal CCAO data models and functions. See [departmental SOPs](https://gitlab.com/groups/ccao-data-science---modeling/-/wikis/SOPs/Open%20Data) for a description of currently curated open data assets. See departmental [data documentation](https://gitlab.com/groups/ccao-data-science---modeling/-/wikis/Data/SQL%20Database%20Guide) for metadata and field definitions.
